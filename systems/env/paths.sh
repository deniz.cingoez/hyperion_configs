#!/usr/bin/env bash

export prefix="/vol/tiago/melodic-robocup/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"

# Path to bonsai config
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${vdemo_prefix}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${vdemo_prefix}/opt/bonsai_robocup_exercise/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${vdemo_prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"